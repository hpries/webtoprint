<?php

use hannespries\Web2Print\TemplateTypes;
use hannespries\Web2Print\Web2Print;
use hannespries\Web2Print\Web2PrintInput;
use PHPUnit\Framework\TestCase;

class Input implements Web2PrintInput {

    public function getData()
    {
        return ['data' => ['Miki', 'Merle', 'Finja', 'Ronja', 'Fetty']];
    }

    public function getTemplateType(): string
    {
        return TemplateTypes::FILE;
    }

    public function getTemplate()
    {
        return __DIR__ . '/index.html.twig';
    }

    public function getPDFOutputPath(): string
    {
        return __DIR__ . '/out.pdf';
    }

    public function getPageSize(): ?string
    {
        return null;
    }

    public function getPageOrientation(): ?string
    {
        return null;
    }
}

class InputM implements Web2PrintInput {

    public function getData()
    {
        return ['data' => ['Miki', 'Merle', 'Finja', 'Ronja', 'Fetty']];
    }

    public function getTemplateType(): string
    {
        return TemplateTypes::FILE;
    }

    public function getTemplate()
    {
        return __DIR__ . '/index.html.twig';
    }

    public function getPDFOutputPath(): string
    {
        return __DIR__ . '/outm.pdf';
    }

    public function getPageSize(): ?string
    {
        return null;
    }

    public function getPageOrientation(): ?string
    {
        return null;
    }
}

class PDFTest extends TestCase{
    public function test_pdf() {
        $input = new Input();
        $out = Web2Print::render($input);
        $this->assertEquals($input->getPDFOutputPath(), $out);
    }

    public function test_mpdf() {
        $input = new InputM();
        $out = Web2Print::render($input, ['pdfrenderer' => 'mpdf']);
        $this->assertEquals($input->getPDFOutputPath(), $out);
    }
}