<?php
namespace hannespries\Web2Print;

class TemplateTypes {
    const STRING = 'string';
    const FILE = 'file';
    const DIR = 'dir';
}