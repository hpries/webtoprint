<?php
namespace hannespries\Web2Print;

interface Web2PrintInput {
    public function getData();
    public function getTemplateType(): string;
    public function getTemplate();
    public function getPDFOutputPath(): string;

    public function getPageSize(): ?string;
    public function getPageOrientation(): ?string;
}