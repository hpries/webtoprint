<?php
namespace hannespries\Web2Print;

use Dompdf\Dompdf;
use Mpdf\Mpdf;
use Twig\Environment;
use Twig\Loader\ArrayLoader;
use Twig\Loader\FilesystemLoader;

class Web2Print {
    private static function addTwigExtensions(Environment $twig): Environment {
        //TODO push events-lib to packagist
        return $twig;
    }

    public static function render(Web2PrintInput $input, array $options = []): ?string {
        $loader = null;
        $start = 'index.html.twig';
        if($input->getTemplateType() == TemplateTypes::STRING) {
            $loader = new ArrayLoader([$start => $input->getTemplate()]);
        }
        else if($input->getTemplateType() == TemplateTypes::DIR) {
            $loader = new FilesystemLoader([$input->getTemplate()]);
        }
        else if ($input->getTemplateType() == TemplateTypes::FILE) {
            $loader = new ArrayLoader([$start => file_get_contents($input->getTemplate())]);
        }
        $twig = new Environment($loader);
        $twig = self::addTwigExtensions($twig);

        $html =  $twig->render($start, array_merge($input->getData(), ['_options' => $options]));

        $path = null;
        if(isset($options['pdfrenderer']) && $options['pdfrenderer'] === 'mpdf') {
            $mpdf = new Mpdf(['orientation' => $input->getPageOrientation() == 'landscape' ? 'L' : null]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($input->getPDFOutputPath());
            $path = $input->getPDFOutputPath();
        }
        else {
            $dompdf = new Dompdf();
            $dompdf->loadHtml($html);

            $dompdf->setPaper($input->getPageSize() ?? 'A4', $input->getPageOrientation() ?? 'landscape');
            $dompdf->render();

            var_dump($input->getPDFOutputPath());
            file_put_contents($input->getPDFOutputPath(), $dompdf->output());
            $path = $input->getPDFOutputPath();
        }

        return $path;
    }
}